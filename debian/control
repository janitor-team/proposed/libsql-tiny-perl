Source: libsql-tiny-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libsql-tiny-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libsql-tiny-perl.git
Homepage: https://metacpan.org/release/SQL-Tiny

Package: libsql-tiny-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Description: simple SQL-building library
 SQL::Tiny is a very simple SQL-building library. It's not for all SQL needs,
 only the very simple ones.
 .
 SQL::Tiny is for generating SQL code for simple cases. It doesn't handle
 JOINs. It doesn't handle GROUP BY. It doesn't handle subselects. It's only
 for simple SQL.
 .
 The trade-off for that brevity of code is that SQL::Tiny has to make new SQL
 and binds from the input every time. You can't cache the SQL that comes back
 from SQL::Tiny because the placeholders could vary depending on what the
 input data is. Therefore, you don't want to use SQL::Tiny where speed is
 essential.
